package week4.day1;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

public class Vendors {
	
	@Test
	public void vendorSearch() throws InterruptedException {
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver =  new ChromeDriver();
		driver.get("https://acme-test.uipath.com/account/login");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		
		driver.findElementById("email").sendKeys("preethymhn@gmail.com");
		driver.findElementById("password").sendKeys("Mannu05&");
		driver.findElementById("buttonLogin").click();
		
		Thread.sleep(5000);
		Actions action = new Actions(driver);
		WebElement element = driver.findElementByXPath("//i[@class='fa fa-truck']/parent::*");
		action.moveToElement(element).build().perform();
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[text()='Search for Vendor']")));
		WebElement search = driver.findElementByXPath("//a[text()='Search for Vendor']");
		search.click();
		
		driver.findElementById("vendorTaxID").sendKeys("RO892123");
		driver.findElementById("buttonSearch").click();
		String text = driver.findElementByXPath("//table/tbody/tr[2]/td").getText();
		System.out.println("The name of the Vendor is: !" + text);
		driver.close();	
		
	}

	}
